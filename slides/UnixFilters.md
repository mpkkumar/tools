% Unix filters.
% Piyush P Kurur
% February 9, 2015

# Basics

## What is a filter.

- A filter is a program that performs some action to its input.

- Examples are grep, sed, awk,

# Main concepts.

## Regular expressions.

- Most filters work with regular expressions

- Regular expressions are built as follows.

	- `a*`, `a+`             - many a's
	- `[0-9]`, `[a-zA-Z]`    - character ranges
	- `^`,`$`                - start and end of the line
	- `.` (dot)              - any character.


## Example use of regular expressions.

- A C identifier        :  `[_a-zA-Z][_a-zA-Z0-9]*`
- Line starting with a #:  `^#`
- A blank like          : `^[ ]*$`


# Grep, sed and awk : The holy trinity

## What do the do?

- grep searches for a pattern
- sed does simple stream editing
- awk - good for column based input.

## Let us look at a sed examples.

- Replace a with b

. . .

```sed
	sed 's/james/007/'
	sed 's/james/007/g'
```

- Remove trailing spaces

. . .

```sed
sed 's/[ \t]+$//'
```

- The above example has a problem

. . .

```sed
sed -r 's/[ \t]+$//'
```

## Awk

- Input is treated as a list of records

- Each record is a set of fields

- Awk gives facilities to do operations on fields.

## General structure of an awk program.

- pattern { action }

- two special patterns BEGIN and END

- null pattern means match anything

. . .

```bash
awk -F ':' '{print $1:$NF} ' < /etc/passwd
```

## Computing marks.

```awk
#!/usr/bin/awk -f

# We want to add up the marks of students where the fields are separated by |
# Format Name | Marks 1 | Marks2 |
#
BEGIN { total= 0; FS="|" }
      {
	  sum = $2 + $3;
	  print $0, "|", sum;
	  total += sum;
      }
END   { print "Average |", (total/NR)}

```
